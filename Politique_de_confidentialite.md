# **Engagement de confidentialité**

La protection de votre vie privée est importante pour **Panga**. C’est la raison pour laquelle nous avons élaboré un Engagement de confidentialité qui régit la manière dont nous recueillons, utilisons, transférons et stockons vos données personnelles.

## **I. Définitions**

Dans le présent engagement, les mots ou expressions auront la signification suivante.

« Panga »/« Nous » désigne la société Panga, SAS au capital de 184 140€, immatriculée au RCS de La Rochelle sous le numéro 813 476 660.

« Vous » / « Votre » / « Vos » désigne la personne physique qui utilise nos sites et/ou nos services et/ou notre application, qu’importe le dispositif technique, pour son propre usage ou pour un tiers.

« Partenaires » / « Tiers » désigne les personnes physiques ou morales, les autorités publiques et les institutions, les services publics, les services et les produits, les applications et les sites web, qui ne sont pas directement contrôlés ou gérés par Panga, mais possèdent une relation contractuelle avec Panga.

« Notre site » désigne le site de présentation de l'activité de **[Panga](https://www.panga.fr)**.

« NeHo » désigne l'application ayant pour objet de délivrer les services et informations en temps réel au sein du bâtiment sur une tablette ou un smartphone.

« Utilisateur(s) » désigne tout utilisateur de l'application ayant accès aux services proposés par la société Panga.

« Données à caractère personnel » désigne toutes données identifiant directement ou indirectement une personne physique, « tel qu’un nom, un numéro d’identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale » selon le Règlement Européen 2016/679 (Art.4-1 du RGDP). Votre nom, votre pseudonyme, votre adresse de courrier électronique ou votre adresse IP sont des données personnelles.

« Consentement » de l'utilisateur désigne, toute manifestation de volonté, libre, spécifique, éclairée et univoque par laquelle l'utilisateur concerné accepte, par une déclaration ou par un acte positif clair, que des données à caractère personnel le concernant fassent l'objet d'un traitement.

« Traitement des données » désigne « toute opération ou tout ensemble d’opérations effectuées ou non à l’aide de procédés automatisés et appliquées à des données ou des ensembles de données à caractère personnel, telles que la collecte, l’enregistrement, l’organisation, la structuration, la conservation, l’adaptation ou la modification, l’extraction, la consultation, l’utilisation, la communication par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l’interconnexion, la limitation, l’effacement ou la destruction » (Art. 4-2 du RGPD).

## **II. Principe encadrant la collecte des données**

En publiant cet engagement, nous définissons plusieurs objectifs :

* La confiance, qui impose que nous détaillions clairement la manière dont nous collectons ces données.

* La transparence, qui demande que nous expliquions pourquoi et comment nous traitons ces données, au-delà même des exigences de la législation.

* La sécurité, qui ne passe pas seulement par le chiffrement, mais aussi et surtout par la réduction de l’étendue de la collecte des données au strict nécessaire, et l’élaboration de processus internes d’accès aux données.

Conformément à la législation en vigueur, nous collectons des données avec votre consentement ou lors de l'exécution d'un contrat, exprimé lors de votre souscription par le biais de la signature d'un contrat à un service proposé par Panga.

## **III. Collecte et utilisation des données personnelles**

Nous nous engageons à ne collecter que les données personnelles nécessaires au bon fonctionnement des services que nous vous proposons.

Il peut vous être demandé de communiquer vos données personnelles lors de la souscription à un service proposé par Panga. Panga et ses partenaires peuvent s’échanger ces données personnelles et les utiliser conformément au présent engagement de confidentialité.

### **Données personnelles que nous recueillons**

Lorsque vous devenez utilisateur de NeHo et des services proposés par Panga, nous devons collecter tout un ensemble d’informations, telles que votre nom, prénom, votre adresse postale, votre numéro de téléphone, votre adresse e-mail, votre adresse IP, les commentaires et discussions, les photos,vidéos et enregistrement vocaux que vous émettez ainsi que les données relatives aux capteurs et objets connectés (sonde de température, thermostat, éclairages, ...) que vous possédez, etc.

Lorsque vous partagez du contenu avec d'autres utilisateurs via des services proposés par Panga,
Panga collecte les informations nécessaires au bon fonctionnement de ces services tels que les
messages émis et reçus ainsi que les interlocuteurs. Panga utilisera ces informations pour répondre
à vos demandes et pour fournir les produits ou services appropriés. Nous prenons soin d'anonymiser toutes vos données afin qu'elles ne permettent pas de retrouver l'identité de leur émetteur.

### **Utilisation de vos données personnelles**

Nous pouvons traiter vos données personnelles dans les objectifs décrits dans cet engagement de confidentialité avec votre consentement, pour nous conformer à une obligation légale à laquelle Panga est soumise, pour l’exécution d’un contrat auquel vous avez souscrit, afin de protéger vos intérêts vitaux, ou lorsque nous estimons que cela est nécessaire pour atteindre les objectifs légitimes poursuivis par Panga ou un tiers à qui nous pouvons être amenés à divulguer ces données.

* Les données personnelles que nous recueillons nous permettent de vous informer des dernières annonces produits, des mises à jour logicielles et des événements Panga à venir vous concernant.

* Nous utilisons également vos données personnelles pour créer, développer, utiliser, livrer et améliorer nos produits, services, contenus et à des fins de prévention des pertes et de lutte contre la fraude. Nous pouvons aussi utiliser vos données personnelles dans des objectifs de sécurité des comptes et réseaux, notamment afin de protéger nos services pour le bénéfice de tous nos utilisateurs, ainsi que filtrer et analyser tout contenu chargé pour nous assurer qu’il ne contient pas de contenus illégaux, tels que des abus sexuels sur mineurs. Nous limitons notre utilisation des données à des fins de lutte contre la fraude aux données strictement nécessaires et dans le cadre de nos intérêts légitimes estimés afin de protéger nos clients et nos services.

* De temps en temps, nous pouvons utiliser vos données personnelles pour envoyer des
notifications importantes, telles que la communication des modifications apportées à nos
conditions d’utilisation et politiques. Ces informations étant importantes pour vos relations
avec Panga, vous ne pouvez pas vous opposer à la réception de ces communications.
* Nous pouvons également utiliser les données personnelles à des fins internes, par exemple
pour des audits, analyses de données et recherches dans le but d’améliorer les produits,
services et communications clients de Panga.
* Toutes les données que nous collectons ne seront stockées que pour une durée maximale de
5 ans après désinstallation de la solution Panga et ne seront en aucun cas revendues.

### **Sécurité des données**

Nous mettons tout en œuvre pour assurer la sécurité de vos données. Les communications avec nos
sites, nos services et nos applications sont chiffrées. Les communications avec nos partenaires sont
chiffrées et effectuées sur un canal sécurisé. L’accès à nos bases de données et nos outils
d’administration est strictement contrôlé et restreint à certains membres de notre équipe.
Au cas où nous serions victimes d’une faille ou d’une perturbation compromettant la sécurité de vos
données personnelles, nous nous engageons à informer la CNIL dans les conditions décrites par
l’article 33 du Règlement Européen 2016/679.
Nous vous informerons, ainsi que nos partenaires concernés le cas échéant, dans un délai de 24
heures en suivant les conditions décrites par l'article 34 du même Règlement.

### **Portabilité et suppression des données**

En plus du droit d’être informé sur l’utilisation de vos données privées, la législation vous confère un droit d'accès, un droit de rectification et un droit à l’effacement, droit à la portabilité des données
personnelles conformément aux articles 15, 16, 17, 20 et 21 du RGDP.
Pour exercer vos droits merci de contacter notre délégué à la protection des données via **[notre adresse mail](mailto:contact@panga.fr)**.

Vous conservez bien sûr vos droits de rectification et de suppression des éventuelles données personnelles que contiendraient vos commentaires et discussions.

### **Modification de cet engagement**

Cette politique est un document vivant, susceptible d’être revu à tout moment et sans préavis. Vous pouvez consulter l’historique des modifications sur le **[GitLab](https://gitlab.com/panga17/politique-de-confidentialite)**.

## **Panga, 1 Rue Alexander Fleming, 17000, La Rochelle, immatriculée n° 813 476 660 par le RCS de La Rochelle et déclarée auprès de la CNIL sous le n° 2024039**
